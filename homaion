#!/bin/bash
# Homaion
# A simple script to load Persian subtitles in video players properly.
# Author: Ma$oud
# License: GPL v3+

confDir="$HOME/.config/Homaion"
confFile="$HOME/.config/Homaion/config"
player=$(grep "Player=" "$confFile" 2> /dev/null | cut -d"=" -f2 )

showHelp(){
	echo -e "Homaion\n\tSimple script to load persian subtitles in video players properly"
	echo -e "Usage:\n\thomaion [-h] <video-path>"
}

while getopts :h opt; do
	case $opt in
		h	)	showHelp && exit;;
		*	)	echo "Invaild argument!" && exit;;
	esac
done

if [ ! "$player" ]; then
	player=$(homaionconfig)
	if [ "$?" -eq 1 ]; then
		exit
	fi
fi

videoFile="$@"

if [ ! "$videoFile" ]; then
	videoFile=$(zenity --file-selection --title="Select Video" 2> /dev/null)
fi

if [ ! "$videoFile" ]; then
	echo "No File Selected!"
	exit
fi

videoName=$(basename "$videoFile")
videoNameNoExt=$(echo $videoName | rev | cut -b 5- | rev)
videoFileDir=$(dirname "$videoFile")
subName=$(ls "$videoFileDir" | grep -F -v "$videoName" | grep -F "$videoNameNoExt" | grep -e srt -e sub -e gsub -e aqt -e jss -e pjs -e psb -e rt -e smi -e stl -e ssf -e ssa -e ass -e usf | head -n1)

if [ ! "$subName" ]; then
	zenity --question --text="Sorry!\nI can't locate subtitle file. \nDo you want locate it manualy?" 2> /dev/null
	if [ $? -eq 0 ]; then
		subFile=$(zenity --file-selection --filename="$videoFile" --title="Select Subtitle" 2> /dev/null)
	else
		exit
	fi
else
	subFile="$videoFileDir/$subName"
fi

subFileEncoding=$(file -b "$subFile" | cut -d" " -f2)

if [ "$subFileEncoding" == "extended-ASCII" ]; then
	case $player in 
		MPV		)	mpv "$videoFile" --sub-file="$subFile" --sub-codepage=utf8:cp1256;;
		MPlayer	)	mplayer "$videoFile" -sub "$subFile"  -subcp "windows-1256";;
		SMPlayer)	
			sed -i "s/subcp=.*/subcp=CP1256/" "$HOME/.config/smplayer/smplayer.ini"
			smplayer "$videoFile" -sub "$subFile";;
		Vlc		)	vlc "$videoFile" --sub-file="$subFile" --subsdec-encoding=CP1256;;
	esac

elif [ "$subFileEncoding" == "UTF-16" ]; then
	case $player in 
		MPV		)	mpv "$videoFile" --sub-file="$subFile";;
		MPlayer	)	mplayer "$videoFile" -sub "$subFile";;
		SMPlayer)	
			sed -i "s/subcp=.*/subcp=CP1256/" "$HOME/.config/smplayer/smplayer.ini"
			smplayer "$videoFile" -sub "$subFile";;
		Vlc		)	vlc "$videoFile" --sub-file="$subFile" --subsdec-encoding=UTF-16LE;;
	esac

else
	case $player in 
		MPV		)	mpv --sub-file="$subFile" "$videoFile";;
		MPlayer	)	mplayer "$videoFile" ;;
		SMPlayer)	
			sed -i "s/subcp=.*/subcp=UTF-8/" "$HOME/.config/smplayer/smplayer.ini"
			smplayer "$videoFile" -sub "$subFile";;
		Vlc		)	vlc "$videoFile" --sub-file="$subFile";;
	esac
fi

