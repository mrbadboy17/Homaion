# Homaion
Simple script to load persian subtitles in video players properly.  

## Installation
```
git clone https://mrbadboy17@gitlab.com/mrbadboy17/Homaion.git
cd Homaion
chmod +x install
./install
```
_Note: This script depends on zenity:_
- Debian base:  
`sudo apt install zenity`

- Arch base:  
`sudo pacman -Sy zenity`

## Supported Players
SMPlayer, MPlayer, MPV, Vlc.
